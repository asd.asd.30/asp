﻿using System.ComponentModel.DataAnnotations;

namespace ASPWebApi
{
    public class Test
    {
        [Required]
        [StringLength(5)]
        public string Id { get; set; }
    }
}
