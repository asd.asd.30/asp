using Microsoft.AspNetCore.Mvc;

namespace ASPWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogWarning("Warning!!!");

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Test))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(void))]
        public async Task<IActionResult> Post([FromBody]Test test)
        {
            return StatusCode(401);
        }

        [HttpGet("get")]
        public ActionResult<string> Get([FromQuery] string id)
        {
            _logger.LogInformation(id);
            return Ok(id);
        }
    }
}